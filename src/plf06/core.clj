(ns plf06.core
  (:gen-class))

;;Zipmap alfabeto completo
(defn alfabeto
  []
  (let [alfabe (seq "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZaábcdeéfghiíjklmnñoópqrstuúüvwxyz")
        caracter (seq "01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789")
        f (fn [xs ys]
            (sort-by second (zipmap (apply str (concat xs ys)) (range (count (concat xs ys))))))]
    (f alfabe caracter)))
;;Buscar una letra en el alfabeto
(defn buscar
  [xs letra]
  (let [f (fn [ws] (= (str (first ws)) letra))]
    (filter f xs)))
;;Remplazar una letra por su indice+13
(defn remplazar
  [s]
  (let [y   (second (first (buscar (alfabeto) s)))
        pos (fn [] (cond (or (and (>= y 20)
                                  (< y 33))
                             (and (>= y 53)
                                  (< y 66))) (+ 13 (- y 33))
                         (>= y 94) (+ 79 (- y 108))
                         :else    (+ 13 y)))
        f   (fn [] (str (first (nth (alfabeto) (pos)))))
        g   (fn [] (if (empty? (buscar (alfabeto) s)) (str s) (f)))]
    (g)))
;;Aplicar ROT13 a toda la cadena
(defn ROT13
  [s]
  (let [f (fn [c] (apply str (map remplazar (map str c))))]
    (f s)))
;;Utilizar el main
(defn -main
  [& args]
  (if (empty? args)
    (println "Error no se han pasado argumentos") ;; ¿A qué se debe el error? ¿Alguna forma de indicarle al usuario la razón?
    (println (ROT13 (apply str args)))))
